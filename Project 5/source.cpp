#include <cstddef>
#include <string.h>
#include <iostream>
#include <stdlib.h>

using namespace std;

extern "C"
{
	#include "compiler.h"
}

//master ValueNode list, which will store pointers to current values of all variables throughout execution
struct id_list
{
	struct ValueNode* v;
	struct id_list *next;
};

struct id_list* idList = NULL;
struct id_list* id_list_head = NULL;
struct id_list* id_list_tail = NULL;
struct StatementNode* stmt_list_head = NULL;
struct StatementNode* stmt_list_tail = NULL;

//forward declarations
void add_to_id_list(struct id_list*);
void add_to_statement_list(struct AssignmentStatement*);
void parse_id_declaration();
struct ValueNode* find_value_node(char*);
struct StatementNode* parse_body();
struct StatementNode* parse_assign_stmt();
struct StatementNode* parse_stmt_list();
struct StatementNode* parse_stmt();
struct StatementNode* parse_assign_stmt();
struct StatementNode* parse_print_stmt();
struct StatementNode* parse_if_stmt();
struct StatementNode* parse_while_stmt();
struct StatementNode* parse_switch_stmt();
struct StatementNode* parse_case_list();
void parse_id_declaration();

//main
struct StatementNode* parse_generate_intermediate_representation()
{
	//initial function that processes variable declarations
	parse_id_declaration();
	//after parsing variable declarations, each declaration will have been added to our master
	//StatementNode* list as declarations ( = 0). Here we append further data structures created
	//after parsing the body of the program
	stmt_list_tail->next = parse_body();
	//return the list to the compiler back end
	return stmt_list_head;
}

struct StatementNode* parse_body()
{
	struct StatementNode* stl;
	ttype = getToken();
	
	if(ttype != LBRACE){/*syntax error*/}
	else{
		stl = parse_stmt_list();
		return stl;
	}
}

struct StatementNode* parse_stmt_list(){
	//StatementNode*
	struct StatementNode* st;
	//StatementNode*(list)
	struct StatementNode* stl;

	//parse single statement
	st = parse_stmt();
	ttype = getToken();
	if(ttype == ID || ttype == PRINT || ttype == IF || ttype == WHILE || ttype == SWITCH){
		ungetToken();
		//recursively add on the rest of the statements in the program
		stl = parse_stmt_list();
		
		//make sure we are on the tail of the list before appending the new list
		struct StatementNode* iterator = st;
		while(iterator->next != NULL){
			iterator = iterator->next;
		}
		iterator->next = stl;
		
		return st;
	}
	else{

		ungetToken();
		return st;
	}
}

struct StatementNode* parse_stmt(){
	//get token
	ttype = getToken();
	
	if(ttype == ID){

		ungetToken();
		return parse_assign_stmt();

	}
	if(ttype == PRINT){

		return parse_print_stmt();

	}
	if(ttype == WHILE){
		//for parsing while statements, extra after the fact work needs to be done here to connect
		//the while statement's GOTO pointer to the beginning of the while statement
		struct StatementNode* tmp = parse_while_stmt();
		tmp->next = tmp->if_stmt->false_branch;

		struct StatementNode* iterator = tmp->if_stmt->true_branch;
		while(iterator->next->type != GOTO_STMT){
			iterator = iterator->next;
		}
		iterator->next->goto_stmt->target = tmp;

		return tmp;
	}
	if(ttype == IF){

		return parse_if_stmt();

	}
	if(ttype == SWITCH){

		return parse_switch_stmt();

	}
}

struct StatementNode* parse_switch_stmt(){

	//get switch operand
	ttype = getToken();
	char* tmpToken = strdup(token);

	//consume opening {
	ttype = getToken();

	//create StatementNode to house the entire case list
	struct StatementNode* sN = parse_case_list();
	
	//Create NOOP node that will be pointed to by the goto at the end of each case's (tail)false_branch->GoToStatement->target
	//also pointed to by the last case's next
	struct StatementNode* noop = new StatementNode();
	noop->type = NOOP_STMT;

	//create a GotoStatement node that will point to the above NOOP, along with a StatementNode for it
	struct GotoStatement* goTo = new GotoStatement();
	goTo->target = noop;
	struct StatementNode* goStatement = new StatementNode();
	goStatement->type = GOTO_STMT;
	goStatement->goto_stmt = goTo;

	struct StatementNode* iterator = sN;
	struct StatementNode* iterator2 = sN;
	while(iterator->next != NULL){
		if(iterator->type == IF_STMT){
			//add the correct ValueNode* corresponding to the switch var to each case after the fact (was out of scope upon creation of this statement list)
			iterator->if_stmt->condition_operand1 = find_value_node(strdup(tmpToken));
			//point the tail of each iterator->if_stmt->false_branch to the GotoStatement
			iterator2 = iterator->if_stmt->false_branch;
			while(iterator2->next != NULL){
				iterator2 = iterator2->next;
			}
			//iterator2 should now be on the last StatementNode in the false_branch of this IfStatement
			iterator2->next = goStatement;
		}
		iterator = iterator->next;
	} 
	//iterator should now be on the last NOOP pointed to by the last case
	iterator->next = noop;

	//consume closing }
	ttype = getToken();

	return sN;
}

struct StatementNode* parse_case_list(){

	struct StatementNode* sN = new StatementNode();
	struct IfStatement* iS = new IfStatement();
	//get "CASE" or "DEFAULT"
	ttype = getToken();
	if(ttype != CASE && ttype != DEFAULT){
		cout << "CASE or DEFAULT expected" << endl;
		cout << strdup(token) << endl;
		exit(1);
	}

	if(ttype == CASE){
		//get var
		ttype = getToken();
		
		//create ValueNode for var
		struct ValueNode* var = new ValueNode();
		var->value = atoi(token);
		//set left operand to NULL since its true value is not currently in scope (will have to set it later)
		iS->condition_operand1 = NULL;
		iS->condition_operand2 = var;

		//standard
		iS->condition_op = NOTEQUAL;

		//get colon
		ttype = getToken();

		//consume first {
		ttype = getToken();

		//point false branch toward case body (reverse since we are using NOTEQUAL)
		iS->false_branch = parse_stmt_list();

		struct StatementNode* noop = new StatementNode();
		noop->type = NOOP_STMT;
		//point true_branch to noop
		iS->true_branch = noop;
		
		//add IfStatement to StatementNode and point StatementNode->next to same noop
		sN->type = IF_STMT;
		sN->if_stmt = iS;
		sN->next = noop;

		//consume closing }
		ttype = getToken();

		//check for further CASE or DEFAULT
		//if so, proceed recursively
		ttype = getToken();
		if(ttype == CASE || ttype == DEFAULT){
			ungetToken();
			sN->next->next = parse_case_list();
		}
		else{
			ungetToken();
		}

		return sN;
	}

	else if(ttype == DEFAULT){
		//get colon
		ttype = getToken();
		//consume first {
		ttype = getToken();

		sN = parse_stmt_list();

		//consume closing }
		ttype = getToken();

		return sN;

	}
}

struct StatementNode* parse_while_stmt(){

	struct IfStatement* whileStmt = new IfStatement();
	struct StatementNode* sN = new StatementNode();

	//get left operand in conditional
	ttype = getToken();
	
	if(ttype == ID){
		whileStmt->condition_operand1 = find_value_node(strdup(token));
	}
	else{
		struct ValueNode* left = new ValueNode();
		left->value = atoi(token);
		whileStmt->condition_operand1 = left;
	}

	//get relop
	ttype = getToken();
	
	if(ttype == GREATER || ttype == LESS || ttype == NOTEQUAL){
		whileStmt->condition_op = ttype;
	}

	//get right operand in conditional
	ttype = getToken();
	
	if(ttype == ID){
		whileStmt->condition_operand2 = find_value_node(strdup(token));
	}
	else{
		struct ValueNode* right = new ValueNode();
		right->value = atoi(token);
		whileStmt->condition_operand2 = right;
	}

	//consume first {
	ttype = getToken();
	
	whileStmt->true_branch = parse_stmt_list();
	
	//create GotoStatement and StatementNode to hold it
	struct GotoStatement* goTo = new GotoStatement();
	struct StatementNode* goToHolder = new StatementNode();
	goToHolder->type = GOTO_STMT;
	//handle GotoStatement->target in parse_stmt()

	//set the goto_stmt in new StatementNode to new GotoStatement
	goToHolder->goto_stmt = goTo;

	//append new StatementNode holding new GotoStatement to end of whileStmt->true_branch
	struct StatementNode* iterator = new StatementNode();
	iterator = whileStmt->true_branch;
	while(iterator->next != NULL){
		iterator = iterator->next;
	}
	iterator->next = goToHolder;

	//create NOOP node
	struct StatementNode* noop = new StatementNode();
	noop->type = NOOP_STMT;

	//point whileStmt->false branch to newly created NOOP node
	whileStmt->false_branch = noop;

	//add whileStmt (which is really an IfStatement) to StatementNode to be returned
	sN->type = IF_STMT;
	sN->if_stmt = whileStmt;

	//get closing }
	ttype = getToken();

	return sN;

}

struct StatementNode* parse_if_stmt(){

	struct StatementNode* sN = new StatementNode();
	struct IfStatement* iS = new IfStatement();

	//get left operand
	ttype = getToken();
	if(ttype == ID){
		iS->condition_operand1 = find_value_node(strdup(token));
	}
	else{
		struct ValueNode* left = new ValueNode();
		left->value = atoi(token);
		iS->condition_operand1 = left;
	}

	//get relop
	ttype = getToken();
	if(ttype == GREATER || ttype == LESS || ttype == NOTEQUAL)
	{
		iS->condition_op = ttype;
	}

	//get right operand
	ttype = getToken();
	if(ttype == ID){
		iS->condition_operand2 = find_value_node(strdup(token));
	}
	else{
		struct ValueNode* right = new ValueNode();
		right->value = atoi(token);
		iS->condition_operand2 = right;
	}

	//consume first {
	ttype = getToken();

	iS->true_branch = parse_stmt_list();
	struct StatementNode* iterator = new StatementNode();
	iterator = iS->true_branch;
	while(iterator->next != NULL)
	{
		iterator = iterator->next;
	}

	struct StatementNode* noop = new StatementNode();
	noop->type = NOOP_STMT;
	iterator->next = noop;
	iS->false_branch = noop;

	sN->type = IF_STMT;
	sN->if_stmt = iS;
	sN->next = noop;

	//get closing }
	ttype = getToken();

	return sN;
}

struct StatementNode* parse_print_stmt(){

	struct PrintStatement* pS = new PrintStatement();
	ttype = getToken();
	pS->id = find_value_node(strdup(token));

	struct StatementNode* sN = new StatementNode();
	sN->type = PRINT_STMT;
	sN->print_stmt = pS;
	sN->next = NULL;

	//consume semicolon
	ttype = getToken();

	return sN;
}

struct StatementNode* parse_assign_stmt(){

	ttype = getToken();
	
	struct ValueNode* targetId = find_value_node(strdup(token));
	struct ValueNode* operand1;
	struct ValueNode* operand2;
	int op = 0;

	if(targetId == NULL){
		//error: variable not declared
		cout << "error: variable not found" << endl;
	}
	else{
		ttype = getToken();
		if(ttype == EQUAL){
			ttype = getToken();
			if(ttype == ID || NUM){
				if(ttype == ID){
					operand1 = find_value_node(strdup(token));
				}
				else{
					struct ValueNode* numValueLeft = new ValueNode();
					numValueLeft->value = atoi(token);
					operand1 = numValueLeft;
				}
				ttype = getToken();
				
				if(ttype == SEMICOLON){
					struct AssignmentStatement* as = new AssignmentStatement();
					as->left_hand_side = targetId;
					as->operand1 = operand1;
					as->operand2 = operand2;
					as->op = op;

					struct StatementNode* stmt = new StatementNode();
					stmt->type = ASSIGN_STMT;
					stmt->assign_stmt = as;
					stmt->next = NULL;
					
					return stmt;
					//end of this assign statement
				}
				else if(ttype == PLUS || ttype == MINUS || ttype == DIV || ttype == MULT){
					
					op = ttype;
					ttype = getToken();
				
					if(ttype == ID){
						operand2 = find_value_node(strdup(token));
					}
					else if(ttype == NUM){
						struct ValueNode* numValueRight = new ValueNode();
						numValueRight->value = atoi(token);
						operand2 = numValueRight;
					}
				}
			}
		}
	}

	struct AssignmentStatement* as = new AssignmentStatement();
	as->left_hand_side = targetId;
	as->operand1 = operand1;
	as->operand2 = operand2;
	as->op = op;

	struct StatementNode* stmt = new StatementNode();
	stmt->type = ASSIGN_STMT;
	stmt->assign_stmt = as;
	stmt->next = NULL;

	//consume trash semicolon
	ttype = getToken();

	return stmt;
}

struct ValueNode* find_value_node(char* n){

	struct id_list* iterator = id_list_head;
	while(iterator != NULL){
		if(strcmp(iterator->v->name, n) == 0){
			return iterator->v;
		}
		iterator = iterator->next;
	}

	return NULL;
}

//ONLY FOR ID DECLARATION SECTION OF INPUT
//add assignment statement to StatementList
void add_to_statement_list(struct AssignmentStatement* as){

	struct StatementNode* sn = new StatementNode();
	sn->type = ASSIGN_STMT;
	sn->assign_stmt = as;

	if(stmt_list_head == NULL){
		stmt_list_head = sn;
		stmt_list_tail = sn;
	}
	else{
		stmt_list_tail->next = sn;
		stmt_list_tail = sn;
	}
}
void add_to_id_list(struct id_list *idl){
	if(id_list_tail == NULL){
		id_list_head = idl;
		id_list_tail = idl;
	}
	else{
		id_list_tail->next = idl;
		//id_list_tail = idl;
		id_list_tail = id_list_tail->next;
	}
}

//ONLY FOR ID DECLARATION SECTION OF INPUT
void parse_id_declaration(){

	ttype = getToken();
	if(ttype == ID){
		//create new ValueNode for left hand side
		struct ValueNode *v = new ValueNode();
		v->name = strdup(token);
		v->value = 0;

		//create new ValueNode for right hand side (0)
		struct ValueNode *v2 = new ValueNode();
		v2->value = 0;

		//create new AssignmentSatement node, add ValueNode to it
		struct AssignmentStatement *a = new AssignmentStatement();
		a->left_hand_side = v;
		a->operand1 = v2;
		a->operand2 = NULL;
		a->op = 0;

		//create new id_list node, add AssignmentStatement node to it
		struct id_list *i = new id_list();
		i->v = v;
		i->next = NULL;

		//add id_list node to idList
		add_to_id_list(i);
		//add new AssignmentStatement to StatementList
		add_to_statement_list(a);

		//continue to next token
		ttype = getToken();
		
		//if the next token is a comma, continue parsing IDs
		if(ttype == COMMA){
			parse_id_declaration();
		}
		else if (ttype == SEMICOLON)
		{
			return;
		}
		else{/*syntax error*/}
	}
}