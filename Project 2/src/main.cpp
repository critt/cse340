//ASU CSE340 Project 2
//Written by Chris Sims
//begin imports
#include <cstddef>
#include <string.h>
#include <iostream>
extern "C"
{
  #include "lexer.h"
}
using namespace std;
//end imports


//forward declarations
const char* toString(token_type);
//end forward declarations

//struct node decalaration
struct node
{
	int line;
	token_type t_type;
	char current_token[MAX_TOKEN_LENGTH];
	struct node* next;
};

//struct list declaration
struct list
{
	struct node* head;
};

//main linked list class
class LinkedList
{
	private:
		struct list* lList;

	public:
		//constructor
		LinkedList()
		{
			lList = new list;
			lList->head = NULL;
		}

		//create new node, add it to the linked list, and return the new node which is now at head
		//the new node is always placed at the head because it will make reading in reverse order, a requirement, easier
		node* addToList(token_type type, char current[MAX_TOKEN_LENGTH], int l)
		{
		
			node* newNode = new node;
			newNode->line = l;
			newNode->t_type = type;
			strcpy(newNode->current_token, current);
			newNode->next = lList->head;
			lList->head = newNode;
			return newNode;
		}

		//print function for the linked list, which will print in order with regard to the linked list, which is in reverse order with regard to the input
		void print()
		{
			node* i = lList->head;
			while(i)
			{
				cout << i->line << " " << toString(i->t_type) << " " << i->current_token << endl;
				i = i->next;
			}
		}
};

//toString method that returns a string conversion of each member of the typedef enum for printing purposes
const char* toString(token_type t)
{
	switch(t)
	{
		case END_OF_FILE:	return "END_OF_FILE";
		case IF:			return "IF";
		case WHILE:			return "WHILE";
		case DO: 			return "DO";
		case THEN: 			return "THEN";
		case PRINT:			return "PRINT";
		case PLUS: 			return "PLUS";
		case MINUS: 		return "MINUS";
		case DIV: 			return "DIV";
		case MULT: 			return "MULT";
		case EQUAL: 		return "EQUAL";
		case COLON:			return "COLON";
		case COMMA:			return "COMMA";
		case SEMICOLON:		return "SEMICOLON";
		case LBRAC:			return "LBRAC";
		case RBRAC:			return "RBRAC";
		case LPAREN:		return "LPAREN";
		case RPAREN:		return "RPAREN";
		case NOTEQUAL:		return "NOTEQUAL";
		case GREATER:		return "GREATER";
		case LESS:			return "LESS";
		case LTEQ:			return "LTEQ";
		case GTEQ:			return "GTEQ";
		case DOT:			return "DOT";
		case NUM:			return "NUM";
		case ID:			return "ID";
		case ERROR:			return "ERROR";
		default:			return "failed token_type to string conversion";
	}
}

//main function
int main()
{
	//dynamically allocate memory for the linked list
	LinkedList* linkedList = new LinkedList();

	//call getToken() once initially
	getToken();

	//this loop performs designated linked list operations and calls getToken() again at the end of each iteration
	while(t_type != END_OF_FILE)
	{
		if(t_type == ID)
		{
			if(strcmp("cse340", current_token) == 0 || strcmp("programming", current_token) == 0 || strcmp("language", current_token) == 0)
			{
				linkedList->addToList(t_type, current_token, line);
			}
		}
		else if(t_type == NUM)
		{
			linkedList->addToList(t_type, current_token, line);
		}
		getToken();
	}

	//print the linked list, which is already arranged in reverse order
	linkedList->print();

	//free dynamically allocated memory
	delete[] linkedList;
	linkedList = NULL;

	//end execution
	return 0;
}